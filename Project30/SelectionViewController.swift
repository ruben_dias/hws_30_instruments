//
//  SelectionViewController.swift
//  Project30
//
//  Created by TwoStraws on 20/08/2016.
//  Copyright (c) 2016 TwoStraws. All rights reserved.
//

import UIKit

class SelectionViewController: UITableViewController {

    lazy var cachedImagesPath = self.getDocumentsDirectory().appendingPathComponent("cachedImages")
    
    var items = [String]()
    var images = [UIImage]()
	var dirty = false

    override func viewDidLoad() {
        super.viewDidLoad()

		title = "Reactionist"

		tableView.rowHeight = 90
		tableView.separatorStyle = .none
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        
        if let cachedImageNames = try? FileManager.default.contentsOfDirectory(atPath: cachedImagesPath.path) {
            cachedImageNames.forEach {
                let imageNameComponents = $0.split(separator: ".")
                let originalImageName = "\(imageNameComponents[0])-Thumb.\(imageNameComponents[1])"
                
                items.append(originalImageName.replacingOccurrences(of: "Thumb", with: "Large"))
                
                if let path = Bundle.main.path(forResource: originalImageName, ofType: nil) {
                    if let image = UIImage(contentsOfFile: path) {
                        images.append(image)
                    }
                }
            }
        }
        else {
            DispatchQueue.global(qos: .userInitiated).async { [weak self] in
                guard let self = self else { return }
                
                if let tempItems = try? FileManager.default.contentsOfDirectory(atPath: Bundle.main.resourcePath!) {
                    for item in tempItems {
                        if item.range(of: "Large") != nil {
                            self.items.append(item)
                        }
                    }
                }
            
                do {
                    try FileManager.default.createDirectory(at: self.cachedImagesPath, withIntermediateDirectories: true, attributes: nil)
                } catch let error as NSError {
                    NSLog("Unable to create directory: \(error.debugDescription)")
                }
                
                for item in self.items {
                    let imageRootName = item.replacingOccurrences(of: "Large", with: "Thumb")
                    let path = Bundle.main.path(forResource: imageRootName, ofType: nil)!
                    let image = UIImage(contentsOfFile: path)!
                    
                    self.images.append(image)

                    if let jpegData = image.jpegData(compressionQuality: 0.8) {
                        let imageName = String(imageRootName.replacingOccurrences(of: "-Thumb", with: ""))
                        let cachedImagePath = self.cachedImagesPath.appendingPathComponent(imageName)
                        try? jpegData.write(to: cachedImagePath)
                    }
                }
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)

		if dirty {
			tableView.reloadData()
		}
	}

	override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return images.count * 10
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        let imageIndex = indexPath.row % items.count
        let originalImage = images[imageIndex]
        
        let renderRect = CGRect(origin: .zero, size: CGSize(width: 90, height: 90))
        let renderer = UIGraphicsImageRenderer(size: renderRect.size)

        let roundedImage = renderer.image { ctx in
            ctx.cgContext.addEllipse(in: renderRect)
            ctx.cgContext.clip()

            originalImage.draw(in: renderRect)
        }
        
		cell.imageView?.image = roundedImage

        cell.imageView?.layer.shadowColor = UIColor.black.cgColor
        cell.imageView?.layer.shadowOpacity = 1
        cell.imageView?.layer.shadowRadius = 10
        cell.imageView?.layer.shadowOffset = CGSize.zero
        cell.imageView?.layer.shadowPath = UIBezierPath(ovalIn: CGRect(origin: .zero, size: CGSize(width: 90, height: 90))).cgPath

		let defaults = UserDefaults.standard
		cell.textLabel?.text = "\(defaults.integer(forKey: items[imageIndex]))"

		return cell
    }

	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let vc = ImageViewController()
        
		vc.image = items[indexPath.row % items.count]
		vc.owner = self

		dirty = false

		navigationController!.pushViewController(vc, animated: true)
	}
}
